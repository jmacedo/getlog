﻿$(document).ready(function () {
    document.getElementById('data').valueAsDate = new Date();
    document.getElementById('data-fim').valueAsDate = new Date();

});

function gerar() {
    if ($("#operador").val() === "") {
        swal.fire({
            title: "Preencha o ID OPERADOR",
            type: "error",
            allowOutsideClick: false
        });

        return;
    }


    if ($("#tipoRequisicao").val() === "Backend") {
        backend();
    }
    else {
        frontend();
    }
}

function backend() {
    $(".dados").html("");

    var operador = $("#operador").val();
    var date = new Date($("#data").val());
    var dateFim = new Date($("#data-fim").val());

    var day = date.getDate() + 1;
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    var dayFim = dateFim.getDate() + 1;
    var monthFim = dateFim.getMonth() + 1;
    var yearFim = dateFim.getFullYear();

    var datainicio = month + "/" + day + "/" + year + " 03:00:01";
    var datafim = monthFim + "/" + (dayFim) + "/" + yearFim + " 02:59:59";

    swal.fire({
        title: "Carregando...",
        text: "Por favor aguarde",
        imageUrl: "load.gif",
        showConfirmButton: false,
        allowOutsideClick: false
    });

    $.ajax({
        url: "http://www.jadlog.com.br/mobileapp/api/v1/Logger/GetLog?operador=" + operador + "&dataInicio=" + datainicio + "&dataFim=" + datafim,
        cache: false,
        success: function (result) {

            swal.close();


            if (result.data === null) {
                Swal.fire({
                    title: 'Nenhum resultado encontrado',
                    footer: ''
                });
                return;
            }

            if (result.data.length === 0) {
                Swal.fire({
                    title: 'Nenhum resultado encontrado',
                    footer: ''
                });
                return;
            }

            for (var i = 0; i < result.data.length; i++) {
                push(format(
                    result.data[i].tipo,
                    result.data[i].dataHora,
                    result.data[i].dados,
                    result.data[i].dadosApp,
                    result.data[i].requisicao,
                    result.data[i].respostas,
                    JSON.stringify(result.data[i]),
                ));
            }

        }
    });
}

function frontend() {
    $(".dados").html("");

    var operador = $("#operador").val();
    var date = new Date($("#data").val());
    var dateFim = new Date($("#data-fim").val());

    var day = date.getDate() + 1;
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    var dayFim = dateFim.getDate() + 1;
    var monthFim = dateFim.getMonth() + 1;
    var yearFim = dateFim.getFullYear() + 1;

    var datainicio = month + "/" + day + "/" + year + " 03:00:01";
    var datafim = monthFim + "/" + (dayFim) + "/" + yearFim + " 02:59:59";

    var pdata = { "DataInicio": datainicio, "DataFIm": datafim, "OperadorId": operador };

    swal.fire({
        title: "Carregando...",
        text: "Por favor aguarde",
        imageUrl: "load.gif",
        showConfirmButton: false,
        allowOutsideClick: false
    });

    $.ajax({
        url: "http://www.jadlog.com.br/mobileapp/api/v1/Logger/logs",
        data: JSON.stringify(pdata),
        method: "POST",
        dataType: 'json',
        contentType: 'application/json',
        cache: false,
        success: function (result) {


            swal.close();

            if (result.data === null) {
                Swal.fire({
                    title: 'Nenhum resultado encontrado',
                    footer: ''
                });
                return;
            }

            if (result.data.length === 0) {
                Swal.fire({
                    title: 'Nenhum resultado encontrado',
                    footer: ''
                });
                return;
            }

            for (var i = 0; i < result.data.length; i++) {
                push(format(
                    "Info",
                    result.data[i].dataHora,
                    result.data[i].descricao,
                    "",
                    "",
                    "",
                    "",
                ));
            }

        }
    });
}

function request(obj) {
    Swal.fire({
        title: 'Request',
        text: $(obj).parent().find(".request").html(),
        footer: ''
    });
}

function response(obj) {
    Swal.fire({
        title: 'Response',
        text: $(obj).parent().find(".response").html(),
        footer: ''
    });
}

function push(format) {
    $(".dados").append(format);
}

function format(tipo, data, dados, dadosApp, request, response, log) {

    data = new Date(data).getHours() + ":" + new Date(data).getMinutes() + ":" + new Date(data).getMilliseconds();

    var img = "<img src='info.png' style='width:25px;margin-top:5px;'>";
    var color = "cold";

    if (tipo === "Error" || tipo === null) {
        img = "<img src='error.png' style='width:25px;margin-top:5px;'>";
        color = "alert";
    }

    return "<tr class='" + color + "'>"
        + "<td  style='width: 40px;'>" + img + "</td>"
        + "<td>" + data + "</td>"
        + "<td><div style='overflow-wrap: break-word;'>" + dados + "</div></td>"
        + "<td>"
        + "<button class='form-control btn-info' onclick='request($(this))'>+</button>"
        + "<div class='request'>" + dadosApp + "</div>"
        + "</td>"
        + "<td>"
        + "<button class='form-control btn-info' onclick='request($(this))'>+</button>"
        + "<div class='request'>" + request + "</div>"
        + "</td>"
        + "<td>"
        + "<button class='form-control btn-info' onclick='response($(this))'>+</button>"
        + "<div class='response'>" + response + "</div>"
        + "</td>"
        + "<td>"
        + "<button class='form-control btn-info' onclick='response($(this))'>+</button>"
        + "<div class='response'>" + log + "</div>"
        + "</td>"
        + "</tr>";
}