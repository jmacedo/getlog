﻿$(document).ready(function () {
    saudeServicos();

    setInterval(function () {
        $(".status").addClass("heartBeat");

        setTimeout(function () {
            $(".status").removeClass("heartBeat");
        }, 300);
    }, 700);
});

function saudeServicos(){
        $.ajax({
            url: "http://www.jadlog.com.br/mobileapp/api/health",
            cache: false,
            success: function (result) {
                if (result.data.service != "Ok") {
                    $("#servico").attr("src", "nok.png");
                }

                if (result.data.mongo_Connection === false) {
                    $("#mongo").attr("src", "nok.png");
                }

                $("#apiversion").html("API VERSION " + result.data.api_version);
            }
        });
}